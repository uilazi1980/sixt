package de.uran.sxt.utils

import android.content.res.Resources
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import de.uran.sxt.delegates.CarDelegate
import de.uran.sxt.model.CarModel

//region GoogleMaps extensions
fun GoogleMap.addCarMarker(carModel: CarDelegate.CarDelegateData, icon: Drawable): Marker? {
    return addMarker(
        carModel.carModel.run {
            MarkerOptions()
                .position(LatLng(carModel.carModel.latitude, longitude))
                .title("< $name($modelName-$make)")
                .snippet("Fuel(%):${fuelLevel.toFloat() * 100}")
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        icon.toBitmap(
                            40.px,
                            40.px
                        )
                    )
                )
        }
    )?.also {
        it.tag = carModel
        if (carModel.isSelected) {
            it.showInfoWindow()
            moveCameraToCar(carModel.carModel, 15f)
        }
    }
}

fun GoogleMap.moveCameraToCar(carModel: CarModel, zoom: Float = 15f) {
    moveCamera(
        CameraUpdateFactory.newLatLngZoom(
            LatLng(
                carModel.latitude,
                carModel.longitude
            ),
            zoom
        )
    )
}

fun GoogleMap.initCarList(
    carList: List<CarDelegate.CarDelegateData>,
    activity: FragmentActivity,
    failIcon: Drawable
) {
    carList.forEach {
        Glide.with(activity)
            .asDrawable()
            .load(it.carModel.carImageUrl)
            .into(object : CustomTarget<Drawable>() {
                override fun onLoadFailed(errorDrawable: Drawable?) {
                    addCarMarker(it, failIcon)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }

                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    addCarMarker(it, resource)
                }

            })
    }
}
//endregion

//region Int extensions
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

//endregion
package de.uran.sxt.delegates

import android.graphics.Color
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import de.uran.sxt.R
import de.uran.sxt.databinding.CarTileBinding
import de.uran.sxt.model.CarModel
import de.uran.sxt.model.FuelType
import de.uran.sxt.ui.ImageLoader
import de.uran.sxt.ui.carlist.CarDelegateListener
import de.uran.sxt.utils.dp

object CarDelegate {
    fun create(imageLoader: ImageLoader, listener: CarDelegateListener) =
        adapterDelegateViewBinding<CarDelegateData, CarDelegateData, CarTileBinding>(
            { layoutInflater, parent -> CarTileBinding.inflate(layoutInflater, parent, false) }
        ) {
            bind {
                imageLoader.loadImage(item.carModel.carImageUrl,
                    { binding.carImage.setImageDrawable(it) },
                    { binding.carImage.setImageResource(R.drawable.ic_car) })
                binding.run {
                    if (item.isSelected) {
                        root.cardElevation =
                            context.resources.getDimension(R.dimen.selected_card_elevation)
                        root.strokeColor = Color.LTGRAY
                        root.strokeWidth = 24.dp
                    } else {
                        root.cardElevation =
                            context.resources.getDimension(R.dimen.default_card_elevation)
                        root.strokeWidth = 0
                    }
                    if(item.carModel.fuelType == FuelType.E){
                        fuelLevelImage.setImageResource(R.drawable.ic_batery_level)
                    }else{
                        fuelLevelImage.setImageResource(R.drawable.ic_fuel_icon)
                    }
                    fuelLevel.progress = item.carModel.fuelLevel.toFloat()
                    title.text = "${item.carModel.name} (${item.carModel.modelName} - ${item.carModel.make}) "
                    registrationNumber.text = "Plate#:${item.carModel.licensePlate}"
                }
            }
            binding.root.setOnClickListener {
                listener.onItemClicked(item)
            }
            binding.chevronImage.setOnClickListener {
                listener.onChevronClicked(item)
            }
        }

    data class CarDelegateData(val carModel: CarModel, val isSelected: Boolean = false)
}

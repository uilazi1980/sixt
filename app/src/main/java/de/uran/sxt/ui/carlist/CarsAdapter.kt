package de.uran.sxt.ui.carlist

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import de.uran.sxt.delegates.CarDelegate
import de.uran.sxt.ui.ImageLoader

class CarsAdapter(imageLoader: ImageLoader, listener: CarDelegateListener ) :
    AsyncListDifferDelegationAdapter<CarDelegate.CarDelegateData>(DiffCallback()) {

init {
    delegatesManager.addDelegate(CarDelegate.create(imageLoader, listener))
}
    internal class DiffCallback : DiffUtil.ItemCallback<CarDelegate.CarDelegateData>() {
        override fun areItemsTheSame(
            oldItem: CarDelegate.CarDelegateData,
            newItem: CarDelegate.CarDelegateData
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: CarDelegate.CarDelegateData,
            newItem: CarDelegate.CarDelegateData
        ): Boolean {
            return oldItem.carModel.id == newItem.carModel.id
        }
    }
}
interface CarDelegateListener{
    fun onItemClicked(carModel: CarDelegate.CarDelegateData)
    fun onChevronClicked(carDelegateData: CarDelegate.CarDelegateData)
}


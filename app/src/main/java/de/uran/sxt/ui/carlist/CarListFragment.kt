package de.uran.sxt.ui.carlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.uran.sxt.R
import de.uran.sxt.databinding.FragmentDashboardBinding
import de.uran.sxt.delegates.CarDelegate
import de.uran.sxt.ui.ImageLoader
import de.uran.sxt.model.CarsViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class CarListFragment : Fragment(), CarDelegateListener {

    val carsViewModel: CarsViewModel by sharedViewModel()
    val imageLoader by inject<ImageLoader>()
    val carsAdapter: CarsAdapter by lazy {
        CarsAdapter(imageLoader, this)
    }

    lateinit var binding: FragmentDashboardBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = carsAdapter
        carsViewModel.getCars()
        carsViewModel.carList.observe(viewLifecycleOwner, {
            carsAdapter.items = it
            it.forEachIndexed { index, carDelegateData ->
                if (carDelegateData.isSelected) {
                    binding.recyclerView.scrollToPosition(index)

                }
            }
        })
    }

    override fun onItemClicked(carModel: CarDelegate.CarDelegateData) {
        carsViewModel.selectedCar = carModel.carModel
    }

    override fun onChevronClicked(carDelegateData: CarDelegate.CarDelegateData) {
        onItemClicked(carDelegateData)
        findNavController().navigate(R.id.action_navigation_list_to_navigation_map)
    }

}
package de.uran.sxt.ui.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import de.uran.sxt.R
import de.uran.sxt.databinding.FragmentMapBinding
import de.uran.sxt.delegates.CarDelegate.CarDelegateData
import de.uran.sxt.map.InfoWindow
import de.uran.sxt.model.CarsViewModel
import de.uran.sxt.ui.ImageLoader
import de.uran.sxt.ui.carlist.CarDelegateListener
import de.uran.sxt.ui.carlist.CarsAdapter
import de.uran.sxt.utils.initCarList
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class MapFragment : Fragment(), OnMapReadyCallback, CarDelegateListener {

    private val carsViewModel: CarsViewModel by sharedViewModel()
    private val imageLoader: ImageLoader by inject()
    private lateinit var binding: FragmentMapBinding
    private val adapter by lazy {
        CarsAdapter(imageLoader, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        carsViewModel.getCars()
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.gMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
        //binding.carBottomSheet.hide()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.setOnMarkerClickListener {
            onItemClicked(it.tag as CarDelegateData)
            it.showInfoWindow()
            true
        }
        googleMap.setOnInfoWindowClickListener {
            findNavController().navigate(R.id.action_navigation_map_to_navigation_list)

        }
        googleMap.setInfoWindowAdapter(InfoWindow(requireContext()))
        carsViewModel.carList.observe(viewLifecycleOwner, { list ->
            ResourcesCompat.getDrawable(resources, R.drawable.ic_car, requireContext().theme)?.run {
                googleMap.initCarList(list, requireActivity(), this)
            }
        })
    }

    override fun onItemClicked(carModel: CarDelegateData) {
        carsViewModel.selectedCar = carModel.carModel
    }

    override fun onChevronClicked(carDelegateData: CarDelegateData) {
        //
    }
}
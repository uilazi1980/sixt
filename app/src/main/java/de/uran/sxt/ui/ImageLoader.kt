package de.uran.sxt.ui

import android.content.Context
import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

class ImageLoader(context: Context) {
    private val glide = Glide.with(context)
    fun loadImage(url: String, onLoadAction: (Drawable) -> Unit, onErrorLoadAction: () -> Unit) {
        glide
            .asDrawable()
            .load(url)
            .into(object : CustomTarget<Drawable>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    onLoadAction.invoke(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    onErrorLoadAction.invoke()
                }
            })
    }
}
package de.uran.sxt.network

import de.uran.sxt.model.CarModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface CarsRepository {
    suspend fun startFetchingCars(): List<CarModel>?
}

class CarsRepositoryImpl(val carsService: CarsService) : CarsRepository {
    override suspend fun startFetchingCars(): List<CarModel>? = withContext(Dispatchers.IO) {
        carsService.getCars().body()
    }

}
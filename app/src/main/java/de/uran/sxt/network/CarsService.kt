package de.uran.sxt.network

import de.uran.sxt.model.CarModel
import retrofit2.Response
import retrofit2.http.GET

interface CarsService {
    @GET("/codingtask/cars")
    suspend fun getCars():Response<List<CarModel>>
}
package de.uran.sxt.di

import android.content.Context
import de.uran.sxt.network.CarsRepository
import de.uran.sxt.network.CarsRepositoryImpl
import de.uran.sxt.network.CarsService
import de.uran.sxt.ui.ImageLoader
import de.uran.sxt.model.CarsViewModel
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

val module = module {
    single {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient()
            .newBuilder()
            .addNetworkInterceptor(logger)
            .cache(get())
            .build()
    }

    single {
        val cacheDir = File(get<Context>().cacheDir, "network_cache")
        val size = 10 * 1024 * 1024L
        Cache(cacheDir, size)
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl("https://cdn.sixt.io")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }


    single {
        get<Retrofit>().create(CarsService::class.java)
    }

    single<CarsRepository> {
        CarsRepositoryImpl(
            get()
        )
    }

    viewModel { CarsViewModel(get()) }

    single {
        ImageLoader(get<Context>())
    }

//    single {
//        params -> CarsAdapter(get(),params.get())
//    }
}

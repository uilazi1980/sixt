package de.uran.sxt.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.uran.sxt.delegates.CarDelegate.CarDelegateData
import de.uran.sxt.network.CarsRepository
import kotlinx.coroutines.launch

class CarsViewModel(val carsRepository: CarsRepository) : ViewModel() {
    var selectedCar: CarModel? = null
        set(value) {
            field = value
            getCars()
        }
    private val _carsList = MutableLiveData<List<CarDelegateData>>()
    val carList = _carsList
    fun getCars() {
        viewModelScope.launch {
            carsRepository.startFetchingCars()?.let { carsList ->
                _carsList.postValue(carsList.map { CarDelegateData(it, it.id == selectedCar?.id) })
            }
        }
    }
}
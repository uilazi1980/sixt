package de.uran.sxt.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CarModel(
    val carImageUrl: String,
    val color: String,
    val fuelLevel: Double,
    val fuelType: FuelType,
    val group: String,
    val id: String,
    val innerCleanliness: String,
    val latitude: Double,
    val licensePlate: String,
    val longitude: Double,
    val make: String,
    val modelIdentifier: String,
    val modelName: String,
    val name: String,
    val series: String,
    val transmission: String
) : Parcelable
enum class FuelType{
    P,
    D,
    E
}
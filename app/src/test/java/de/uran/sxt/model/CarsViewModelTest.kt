package de.uran.sxt.model

import de.uran.sxt.InstantExecutorExtension
import de.uran.sxt.network.CarsRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class, InstantExecutorExtension::class)
class CarsViewModelTest {
    @MockK
    lateinit var mockedCar:CarModel

    @MockK(relaxed = true)
    lateinit var carsRepository: CarsRepository

    lateinit var carsViewModel: CarsViewModel

    private val dispatcher = TestCoroutineDispatcher()

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(dispatcher)
        coEvery {
            carsRepository.startFetchingCars()
        } returns listOf(mockedCar)
        every {
            mockedCar.id
        } returns "mockedId"
        carsViewModel = CarsViewModel(carsRepository)
    }

    @Test
    fun `When ewer car is selected then list is broadcasted with marker isSelected `() {
        carsViewModel.selectedCar = mockedCar
        assert(
            carsViewModel.carList.value?.get(0)?.isSelected == true
        )
    }

    @Test
    fun `When ever getCars is called the cars are fetched`() {
        carsViewModel.getCars()
        coVerify(exactly = 1) {
            carsRepository.startFetchingCars()
        }
    }
}